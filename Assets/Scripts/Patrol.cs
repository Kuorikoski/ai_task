﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float speed;
    private float waitTime;
    public float startWaitTime;

    public Transform moveSpot;
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;
    public float lockY;

    // Start is called before the first frame update
    void Start()
    {
        moveSpot.position = new Vector3(Random.Range(minX, maxX), lockY, Random.Range(minZ, maxZ));
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, moveSpot.position, speed * Time.deltaTime);

        if(Vector3.Distance(transform.position, moveSpot.position) < 0.2f)
            if (waitTime <= 0)
            {
                moveSpot.position = new Vector3(Random.Range(minX, maxX), lockY, Random.Range(minZ, maxZ));
                waitTime = startWaitTime;

            } else
            {
                waitTime -= Time.deltaTime;
            }
        {

        }
    }
}


